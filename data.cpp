#include "data.h"
int system__::functCnt = 0;
void system__::show(void)
{
	std::cout << "Nowy system:\n";
	for (auto& x : functionality)
	{
		std::cout << "funkcjonalnosc:" << x.funct << " miesiac: " << x.time << std::endl;
	}
	
	if (!toFill.empty())
	{
		std::cout << "\nzaleglosci:\n";
		for (auto& x : toFill)
		{
			std::cout << "funkcjonalnosc:" << x.funct << " miesiac: " << x.time << " licznik: " << x.cnt << std::endl;
		}
	}
	std::cout << std::endl;
}

int allSystems::readData( std::ifstream & file)
{
	//data format 
	//number of systems
	//describing string
	//nmb of functions
	//funct nmb time
	//....
	//describing string
	//nmb of functions
	//itp
	//alredy constructed 
	int time, funct, max = 0, maxt = 0;
	for (system__& sys : systems)
	{
		std::string desc;
		file >> desc;
		int n;
		file >> n;
		while (n--)
		{
			file >> funct;
			file >> time;
			if (funct > max)
				max = funct;
			if (time > maxt)
				maxt = time;
			sys.getFunct().push_back({ funct, 0, time });
		}
	}
	system__::setCnt(max+1);
	return maxt;
}
void allSystems::update(int currentTime)
{
	for (system__& x : systems)
	{
		int p = rand() % 10 + 1;
		if (x.getFill().empty())
		{
			if (p > probToNot)
			{
				x.getFunct().push_back({ system__::getCnt(),0,currentTime });
				system__::setCnt(1);
			}
		}
		else
		{
			if (p <= probToNew)
			{
				x.getFunct().push_back({ system__::getCnt(),0,currentTime });
				system__::setCnt(1);
				continue;
			}
			else if (p > 10 - probToNot)
				continue;
			switchf(x, currentTime);
		}
	}
}
void allSystems::computeToFill( int currentTime)
{
	int n = systems.size();
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (i != j)
			{
				std::vector<data> & sys1f = systems[i].getFunct();
				std::vector<data>& sys2f = systems[j].getFunct();
				std::vector<data>& fill = systems[j].getFill();
				int m = std::max(sys1f.size(), sys2f.size());
				std::vector<data> diff(m);
				auto end = std::set_difference(sys1f.begin(), sys1f.end(), sys2f.begin(), sys2f.end(), diff.begin(), [](data & a, data & b) {return a.funct < b.funct; });
				std::vector<data> add;
				if (fill.empty())
				{
					for (auto y = diff.begin(); y != end; y++)
						fill.push_back(*y);
				}
				else
					for (auto y = diff.begin(); y != end; y++)
					{
						auto first = fill.begin();
						while (first != fill.end())
						{
							if ((*first).funct == (*y).funct)
							{
								(*first).cnt++;
								(*first).time -= abs((*y).time - (*first).time);//aktualny czas - roznica // time - abs(time - timeold)
								break;
							}
							++first;
						}
						if (first == fill.end())
							add.push_back(*y);
					}
				if (!add.empty())
					fill.insert(fill.end(), add.begin(), add.end());
				std::sort(fill.begin(), fill.end(), [](data & a, data & b) {return a.funct < b.funct; });
			}
		}
	}

}
void allSystems::reg2(system__& sys, int time)
{
	std::vector<data>& system = sys.getFill();
	if (!system.empty())
	{
		auto it = std::min_element(system.begin(), system.end(), [](data & a, data & b) {
			if (a.time < b.time)
				return true;
			else if (a.time == b.time)
				return a.cnt > b.cnt;
			else
				return false; });
		sys.getFunct().push_back({ it->funct,0,time });
		system.erase(it);
		std::sort(sys.getFunct().begin(), sys.getFunct().end(), [](data & a, data & b) { return a.funct < b.funct; });
	}
}
void allSystems::reg3(system__ & sys, int time)
{
	std::vector<data>& system = sys.getFill();
	if (!system.empty())
	{
	auto it = std::min_element(system.begin(), system.end(), [](data & a, data & b) {
		if (a.cnt > b.cnt)
			return true;
		else if (a.cnt == b.cnt)
			return a.time < b.time;
		else
			return false; });
	sys.getFunct().push_back({ it->funct,0,time });
	system.erase(it);
	std::sort(sys.getFunct().begin(), sys.getFunct().end(), [](data & a, data & b) { return a.funct < b.funct; });
	}
}
void allSystems::reg6(system__ & sys, int time)
{
	std::vector<data>& system = sys.getFill();
	if (!system.empty())
	{
	auto it = std::min_element(system.begin(), system.end(), [](data & a, data & b) {
		if (a.time > b.time)
			return true;
		else if (a.time == b.time)
			return a.cnt > b.cnt;
		else
			return false; });
	sys.getFunct().push_back({ it->funct,0,time });
	system.erase(it);
	std::sort(sys.getFunct().begin(), sys.getFunct().end(), [](data & a, data & b) { return a.funct < b.funct; });
	}
}
void allSystems::reg7(system__ & sys, int time)
{
	std::vector<data>& system = sys.getFill();
	if (!system.empty())
	{
	auto it = std::min_element(system.begin(), system.end(), [](data & a, data & b) {
		return a.time < b.time; });
	sys.getFunct().push_back({ it->funct,0,time });
	system.erase(it);
	std::sort(sys.getFunct().begin(), sys.getFunct().end(), [](data & a, data & b) { return a.funct < b.funct; });
	}
}
void allSystems::reg10(system__  & sys, int time)
{
	std::vector<data>& system = sys.getFill();
	if (!system.empty())
	{
	auto it = std::min_element(system.begin(), system.end(), [](data & a, data & b) {
		return a.cnt > b.cnt; });
	sys.getFunct().push_back({ it->funct,0,time });
	system.erase(it);
	std::sort(sys.getFunct().begin(), sys.getFunct().end(), [](data & a, data & b) { return a.funct < b.funct; });
	}
}
void allSystems::reg12(system__ & sys, int time)
{
	if (!sys.getFill().empty())
	{
		int nmb = rand() % sys.getFill().size();
		sys.getFunct().push_back({ sys.getFill()[nmb].funct,0,time });
		sys.getFill().erase(sys.getFill().begin() + nmb);
		std::sort(sys.getFunct().begin(), sys.getFunct().end(), [](data & a, data & b) { return a.funct < b.funct; });
	}
}
void allSystems::switchf(system__& x, int time)
{
		switch (reg)
		{
		case 3:
			reg3(x, time);
			break;
		case 6:
			reg6(x, time);
			break;
		case 7:
			reg7(x, time);
			break;
		case 10:
			reg10(x, time);
			break;
		case 12:
			reg12(x, time);
			break;
		default:
			reg2(x, time);
			break;
		}
}
void allSystems::save(std::ofstream& file)
{
	/*int max = 0;
	for (int i = 0;i<systems.size(); i++)
	{
		file << "system: " << i + 1 << "\tid funkcjonalnosci\tmiesiac wypuszczenia\t";
		if (max < systems[i].getFunct().size())
			max = systems[i].getFunct().size();
	}
	file << std::endl;
	for (int i = 0;i < max; i++)
	{
		for (auto& x : systems)
		{
			if (i >= x.getFunct().size())
			{
				file << "\t\t\t";
				break;
			}
			file << i+1 <<"\t" << x.getFunct()[i].funct << "\t" << x.getFunct()[i].time << "\t";
		}
		file << std::endl;
	}*/
	file << systems.size() <<std::endl;
		for(auto& x : systems)
		{
			std::sort(x.getFunct().begin(), x.getFunct().end(), [](data & a, data & b) { return a.time < b.time; });
			file << x.getFunct().size() << std::endl;
			for (int i = 0; i < x.getFunct().size(); i++)
			{

				file << i + 1 << "\n";
			}
			for (int i = 0; i < x.getFunct().size(); i++)
			{
				file << x.getFunct()[i].funct << "\n";
			}
			for (int i = 0; i < x.getFunct().size(); i++)
			{
				file <<x.getFunct()[i].time << "\n";
			}
		}
		file << std::endl;
}