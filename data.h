
#include <iostream>
#include<vector>
#include<algorithm>
#include<string>
#include <fstream>
#include <stdlib.h> 
#include <time.h> 

struct data
{
	int funct;
	int cnt;
	int time;
};
class system__
{
private:
	static int functCnt;
	std::vector<data> functionality;
	std::vector<data> toFill;
public:
	std::vector<data>& getFill(void) { return toFill; };
	std::vector<data>& getFunct(void) { return functionality; };
	void show(void);
	static void setCnt(int a) { functCnt += a; };
	static int getCnt(void) { return functCnt; };
};
//int system__::functCnt = 0;
class allSystems
{
private:
	int probToNot;
	int probToNew;
	int reg;
	std::vector<system__> systems;
public:
	allSystems(int probn = 1, int probnew = 2, int regg = 2, int n = 1) :probToNot(probn), probToNew(probnew), reg(regg), systems(n) { std::cout << "gitara" << std::endl; };
	int readData( std::ifstream& file);
	void computeToFill( int currentTime);
	void update( int currentTime);
	void reg2(system__& sys, int);
	void reg3(system__& sys, int);
	void reg6(system__& sys, int);
	void reg7(system__& sys, int);
	void reg10(system__& sys, int);
	void reg12(system__& sys, int);
	void show(void) { for(auto& x : systems) x.show(); };
	void switchf(system__& sys, int time);
	void setf(int x) { reg = x; };
	void save(std::ofstream& file);

};
