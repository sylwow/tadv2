﻿// TADY.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include"data.h"

int main()
{
	srand(time(NULL));
	std::ifstream file;
	file.open("datainput2.txt");
	std::ofstream fileout;
	fileout.open("dataoutput.txt");
	int n, miesiace, regola;
	
	std::cout << "podaj liczbe miesiecy symulacji\n";
	std::cin >> miesiace;
	file >> n;
	allSystems systems(1,2,2,n);
	std::cout << "podaj regole\n";
	std::cin >> regola;
	systems.setf(regola);
	int offset = systems.readData(file);
	miesiace += offset;
	for (int i = offset + 1; i < miesiace; i++)
	{
		std::cout << i << std::endl;
		systems.computeToFill(i);
		systems.update(i);
		systems.show();
	}
	systems.save(fileout);
	fileout.close();
	file.close();
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln
